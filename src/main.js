import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VModal from 'vue-js-modal'
import Toasted from 'vue-toasted';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
 
Vue.use(Toasted ,{
  iconPack : 'material' // set your iconPack, defaults to material. material|fontawesome|custom-class
})
Vue.config.productionTip = false
Vue.use(VModal)



new Vue({
  router,
  render: h => h(App)
}).$mount('#app')

